﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ObjectPlacement::Start()
extern void ObjectPlacement_Start_mD2AD0F8BCA428B7405BC1EA41B88C92AF5F782CD ();
// 0x00000002 System.Void ObjectPlacement::Update()
extern void ObjectPlacement_Update_mAC928AF861E60B933B23A0DE489D428FADB12385 ();
// 0x00000003 System.Void ObjectPlacement::UpdatePlacementPose()
extern void ObjectPlacement_UpdatePlacementPose_m721B5094E68F9C0537C05489612D688F654673CF ();
// 0x00000004 System.Void ObjectPlacement::UpdatePlacementIndicator()
extern void ObjectPlacement_UpdatePlacementIndicator_m709169B338C860AC447F0A49A629A932DBF6A740 ();
// 0x00000005 System.Void ObjectPlacement::.ctor()
extern void ObjectPlacement__ctor_m5720F6E73F25203D227560552032536E25D21224 ();
static Il2CppMethodPointer s_methodPointers[5] = 
{
	ObjectPlacement_Start_mD2AD0F8BCA428B7405BC1EA41B88C92AF5F782CD,
	ObjectPlacement_Update_mAC928AF861E60B933B23A0DE489D428FADB12385,
	ObjectPlacement_UpdatePlacementPose_m721B5094E68F9C0537C05489612D688F654673CF,
	ObjectPlacement_UpdatePlacementIndicator_m709169B338C860AC447F0A49A629A932DBF6A740,
	ObjectPlacement__ctor_m5720F6E73F25203D227560552032536E25D21224,
};
static const int32_t s_InvokerIndices[5] = 
{
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	5,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
