﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::UnityARKit_FaceProvider_Initialize()
extern void ARKitFaceSubsystem_UnityARKit_FaceProvider_Initialize_mDC61B1E4243285043F5A1CBC344F725EC0FC9E9F ();
// 0x00000002 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::UnityARKit_FaceProvider_Shutdown()
extern void ARKitFaceSubsystem_UnityARKit_FaceProvider_Shutdown_mCFF708E77F0C69BBCD28FC35D2041DC9288BA76E ();
// 0x00000003 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::UnityARKit_FaceProvider_Start()
extern void ARKitFaceSubsystem_UnityARKit_FaceProvider_Start_m2687BC560F4269B7EB699F64B533FAE0BBE67167 ();
// 0x00000004 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::UnityARKit_FaceProvider_Stop()
extern void ARKitFaceSubsystem_UnityARKit_FaceProvider_Stop_m814D4D3BBF89E7BC4E3F742C5C4A40FA0622CA81 ();
// 0x00000005 System.Void* UnityEngine.XR.ARKit.ARKitFaceSubsystem::UnityARKit_FaceProvider_AcquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void ARKitFaceSubsystem_UnityARKit_FaceProvider_AcquireChanges_m487A56D4C2B7DD7794BE3D90B1F2E4059825D07A ();
// 0x00000006 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::UnityARKit_FaceProvider_ReleaseChanges(System.Void*)
extern void ARKitFaceSubsystem_UnityARKit_FaceProvider_ReleaseChanges_m958F3E3C180B34BC3F07A304DC66F97D80C40489 ();
// 0x00000007 UnityEngine.XR.ARSubsystems.XRFaceSubsystem_IProvider UnityEngine.XR.ARKit.ARKitFaceSubsystem::CreateProvider()
extern void ARKitFaceSubsystem_CreateProvider_m778057F5740ACFB678634459668EFB28B89528C0 ();
// 0x00000008 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::RegisterDescriptor()
extern void ARKitFaceSubsystem_RegisterDescriptor_mC00841747E3532C2A69720ECBA2EA399C657E111 ();
// 0x00000009 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem::.ctor()
extern void ARKitFaceSubsystem__ctor_mCFD2FDAC2B2FD45B7C15B697268F6D9B54691905 ();
// 0x0000000A System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider::.ctor()
extern void Provider__ctor_mABA49E4EB31CCAA6142EBFA66BBA3DAEC2368A5C ();
// 0x0000000B System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider::Start()
extern void Provider_Start_m741BFEEEF73EC59D2AF1349ED0605FB75841C58C ();
// 0x0000000C System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider::Stop()
extern void Provider_Stop_m5B543FECB5190A3E3D5E7BC1D54AB79F413FBAF9 ();
// 0x0000000D System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider::Destroy()
extern void Provider_Destroy_mA64E01AFBEF33DE8935C4E41471A6CC367996B23 ();
// 0x0000000E UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace> UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider::GetChanges(UnityEngine.XR.ARSubsystems.XRFace,Unity.Collections.Allocator)
extern void Provider_GetChanges_m60D7012C099A9C1F30453E0E8CFFED6AE80DBF91 ();
// 0x0000000F System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider_TransformVerticesJob::Execute(System.Int32)
extern void TransformVerticesJob_Execute_mF8FA52D8CCEE36BE2C35B91AC6D22901AFBE7B8C_AdjustorThunk ();
// 0x00000010 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider_TransformUVsJob::Execute(System.Int32)
extern void TransformUVsJob_Execute_mF58A7833E0ADC93843EB5B0D8B7A7058E99FF17E_AdjustorThunk ();
// 0x00000011 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider_Triangle`1::.ctor(T,T,T)
// 0x00000012 System.Void UnityEngine.XR.ARKit.ARKitFaceSubsystem_Provider_TransformIndicesJob::Execute(System.Int32)
extern void TransformIndicesJob_Execute_mB165C91EE3E830C2A087D2025BD747890182A5D2_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[18] = 
{
	ARKitFaceSubsystem_UnityARKit_FaceProvider_Initialize_mDC61B1E4243285043F5A1CBC344F725EC0FC9E9F,
	ARKitFaceSubsystem_UnityARKit_FaceProvider_Shutdown_mCFF708E77F0C69BBCD28FC35D2041DC9288BA76E,
	ARKitFaceSubsystem_UnityARKit_FaceProvider_Start_m2687BC560F4269B7EB699F64B533FAE0BBE67167,
	ARKitFaceSubsystem_UnityARKit_FaceProvider_Stop_m814D4D3BBF89E7BC4E3F742C5C4A40FA0622CA81,
	ARKitFaceSubsystem_UnityARKit_FaceProvider_AcquireChanges_m487A56D4C2B7DD7794BE3D90B1F2E4059825D07A,
	ARKitFaceSubsystem_UnityARKit_FaceProvider_ReleaseChanges_m958F3E3C180B34BC3F07A304DC66F97D80C40489,
	ARKitFaceSubsystem_CreateProvider_m778057F5740ACFB678634459668EFB28B89528C0,
	ARKitFaceSubsystem_RegisterDescriptor_mC00841747E3532C2A69720ECBA2EA399C657E111,
	ARKitFaceSubsystem__ctor_mCFD2FDAC2B2FD45B7C15B697268F6D9B54691905,
	Provider__ctor_mABA49E4EB31CCAA6142EBFA66BBA3DAEC2368A5C,
	Provider_Start_m741BFEEEF73EC59D2AF1349ED0605FB75841C58C,
	Provider_Stop_m5B543FECB5190A3E3D5E7BC1D54AB79F413FBAF9,
	Provider_Destroy_mA64E01AFBEF33DE8935C4E41471A6CC367996B23,
	Provider_GetChanges_m60D7012C099A9C1F30453E0E8CFFED6AE80DBF91,
	TransformVerticesJob_Execute_mF8FA52D8CCEE36BE2C35B91AC6D22901AFBE7B8C_AdjustorThunk,
	TransformUVsJob_Execute_mF58A7833E0ADC93843EB5B0D8B7A7058E99FF17E_AdjustorThunk,
	NULL,
	TransformIndicesJob_Execute_mB165C91EE3E830C2A087D2025BD747890182A5D2_AdjustorThunk,
};
static const int32_t s_InvokerIndices[18] = 
{
	3,
	3,
	3,
	3,
	1504,
	17,
	14,
	3,
	23,
	23,
	23,
	23,
	23,
	1349,
	32,
	32,
	-1,
	32,
};
extern const Il2CppCodeGenModule g_Unity_XR_ARKit_FaceTrackingCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARKit_FaceTrackingCodeGenModule = 
{
	"Unity.XR.ARKit.FaceTracking.dll",
	18,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
