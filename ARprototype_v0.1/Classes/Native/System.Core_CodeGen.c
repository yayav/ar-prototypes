﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000009 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000D System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000010 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000012 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000015 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000019 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001D System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000020 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
static Il2CppMethodPointer s_methodPointers[32] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[32] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000004, { 22, 4 } },
	{ 0x02000005, { 26, 9 } },
	{ 0x02000006, { 35, 7 } },
	{ 0x02000007, { 42, 10 } },
	{ 0x02000008, { 52, 1 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[53] = 
{
	{ (Il2CppRGCTXDataType)2, 12890 },
	{ (Il2CppRGCTXDataType)3, 10301 },
	{ (Il2CppRGCTXDataType)2, 12891 },
	{ (Il2CppRGCTXDataType)2, 12892 },
	{ (Il2CppRGCTXDataType)3, 10302 },
	{ (Il2CppRGCTXDataType)2, 12893 },
	{ (Il2CppRGCTXDataType)2, 12894 },
	{ (Il2CppRGCTXDataType)3, 10303 },
	{ (Il2CppRGCTXDataType)2, 12895 },
	{ (Il2CppRGCTXDataType)3, 10304 },
	{ (Il2CppRGCTXDataType)2, 12896 },
	{ (Il2CppRGCTXDataType)3, 10305 },
	{ (Il2CppRGCTXDataType)3, 10306 },
	{ (Il2CppRGCTXDataType)2, 10704 },
	{ (Il2CppRGCTXDataType)3, 10307 },
	{ (Il2CppRGCTXDataType)2, 10706 },
	{ (Il2CppRGCTXDataType)2, 12897 },
	{ (Il2CppRGCTXDataType)3, 10308 },
	{ (Il2CppRGCTXDataType)2, 10709 },
	{ (Il2CppRGCTXDataType)2, 10711 },
	{ (Il2CppRGCTXDataType)2, 12898 },
	{ (Il2CppRGCTXDataType)3, 10309 },
	{ (Il2CppRGCTXDataType)3, 10310 },
	{ (Il2CppRGCTXDataType)3, 10311 },
	{ (Il2CppRGCTXDataType)2, 10716 },
	{ (Il2CppRGCTXDataType)3, 10312 },
	{ (Il2CppRGCTXDataType)3, 10313 },
	{ (Il2CppRGCTXDataType)2, 10725 },
	{ (Il2CppRGCTXDataType)2, 12899 },
	{ (Il2CppRGCTXDataType)3, 10314 },
	{ (Il2CppRGCTXDataType)3, 10315 },
	{ (Il2CppRGCTXDataType)2, 10727 },
	{ (Il2CppRGCTXDataType)2, 12798 },
	{ (Il2CppRGCTXDataType)3, 10316 },
	{ (Il2CppRGCTXDataType)3, 10317 },
	{ (Il2CppRGCTXDataType)3, 10318 },
	{ (Il2CppRGCTXDataType)2, 10734 },
	{ (Il2CppRGCTXDataType)2, 12900 },
	{ (Il2CppRGCTXDataType)3, 10319 },
	{ (Il2CppRGCTXDataType)3, 10320 },
	{ (Il2CppRGCTXDataType)3, 9882 },
	{ (Il2CppRGCTXDataType)3, 10321 },
	{ (Il2CppRGCTXDataType)3, 10322 },
	{ (Il2CppRGCTXDataType)2, 10743 },
	{ (Il2CppRGCTXDataType)2, 12901 },
	{ (Il2CppRGCTXDataType)3, 10323 },
	{ (Il2CppRGCTXDataType)3, 10324 },
	{ (Il2CppRGCTXDataType)3, 10325 },
	{ (Il2CppRGCTXDataType)3, 10326 },
	{ (Il2CppRGCTXDataType)3, 10327 },
	{ (Il2CppRGCTXDataType)3, 9888 },
	{ (Il2CppRGCTXDataType)3, 10328 },
	{ (Il2CppRGCTXDataType)3, 10329 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	32,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	53,
	s_rgctxValues,
	NULL,
};
