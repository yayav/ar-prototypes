﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class audioManager : MonoBehaviour
{

	private AudioSource audiosource;
	public Sprite audioImageDefault;
	public Sprite audioImageMuted;
    public Image audioUI;

	void Start()
	{
		audiosource = GetComponent<AudioSource>();
		Image audioUI = GetComponent<Image>();
	}

    public void toggleAudio(bool value)
	{
		if (value == true)
		{
			audiosource.volume = 0.5f;
			audioUI.sprite = audioImageDefault;
			var tempColor = audioUI.color;
			tempColor.a = 0.8f;
			audioUI.color = tempColor;
		}
		if (value == false)
		{
			audiosource.volume = 0.0f;
			audioUI.sprite = audioImageMuted;
			var tempColor = audioUI.color;
			tempColor.a = 0.5f;
			audioUI.color = tempColor;
		}
	}
}
