﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation; 
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class multipleObjectsPlacement : MonoBehaviour
{
    public GameObject placementIndicator, menuUI, gameplayUI, butterflies;
	private GameObject curObject;
	private ARRaycastManager arOrigin;
    private Pose placementPose;
    private bool placementPoseIsValid = false;
	public GameObject[] selectableItems;
	private int curItem;
	public Button grassButton, flowerButton;
    public AudioSource audioSource;
    public AudioClip placeJingle, button;
    bool butterflyToggle = true;

    void Start()
    {
		arOrigin = FindObjectOfType<ARRaycastManager>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
			grassButton.onClick.AddListener(() => PlaceObject(0));
			flowerButton.onClick.AddListener(() => PlaceObject(1));
		}

        GameObject[] flowerInstants = GameObject.FindGameObjectsWithTag("Flower");
        int flowerCount = flowerInstants.Length;

        if (flowerCount >= 10 && butterflyToggle == true)
        {
            triggerEvent("butterfly");
            butterflyToggle = false;
        }
    }


    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        arOrigin.GetComponent<ARRaycastManager>().Raycast(screenCenter, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;
            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }

    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

	public void PlaceObject(int curItem)
	{
        audioSource.PlayOneShot(button, 0.5F);
        Instantiate(selectableItems[curItem], placementPose.position, placementPose.rotation);
		//Debug.Log("something is instantiated............");

		if (curItem == 0)
		{
			selectableItems[1].SetActive(false);
			selectableItems[0].SetActive(true);
			//Debug.Log("Grass is selected");
        }
		else if (curItem == 1)
		{
			selectableItems[0].SetActive(false);
			selectableItems[1].SetActive(true);
			//Debug.Log("Flower is selected");
		}
        audioSource.PlayOneShot(placeJingle, 0.5F);
    }

    public void startGame()
    {
        audioSource.PlayOneShot(button, 0.5F);
        menuUI.SetActive(false);
        gameplayUI.SetActive(true);
    }

    private void triggerEvent(string eventName)
    {
        if (eventName == "butterfly")
        {
            Instantiate(butterflies, new Vector3(0, 0, 0), Quaternion.identity);
            var butterflyPar = butterflies.GetComponent<ParticleSystem>();
            butterflyPar.Play();
        }

    }

    public void hideUI(bool value)
    {
        audioSource.PlayOneShot(button, 0.5F);
        if (value == true)
        {
            placementIndicator.SetActive(false);
            gameplayUI.SetActive(false);
            Debug.Log("UI is hidden ----------------------------");
        }
        else if (value == false)
        {
            placementIndicator.SetActive(true);
            gameplayUI.SetActive(true);
            Debug.Log("UI is displayed ----------------------------");
        }
    }
}
