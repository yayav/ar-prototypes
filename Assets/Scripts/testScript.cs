﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testScript : MonoBehaviour
{

    public GameObject[] selectableItems;
    public Button grassButton, flowerButton;
    private int curItem;

    void Start()
    {
    }

    void Update()
    {
        if (grassButton != null && flowerButton != null)
        {
           grassButton.onClick.AddListener(() => PlaceObject(0));
           flowerButton.onClick.AddListener(() => PlaceObject(1));
        }
        
    }

    public void PlaceObject(int curItem)
    {
        Instantiate(selectableItems[curItem], new Vector3(0, 0, 0), Quaternion.identity);
        Debug.Log("something is instantiated............");
    }
}
